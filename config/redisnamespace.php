<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

return [
    'userInfo' => 'user:user_info:user_id_',
    'userToken' => 'user:user_token:token_',
    'codeVerifyLogin' => 'user:verify_login:mobile_',// 验证码登录
    'codeVerifyLoginLimit' => 'user:verify_login_limit:mobile_',// 验证码登录次数限制
    'codeUserRegister' => 'user:user_register:mobile_',// 手机注册验证
    'codeUserRegisterLimit' => 'user:user_register_limit:mobile_',// 手机号码验证码注册次数限制
    'codeUserModifyPassword' => 'user:user_modify_password:mobile_',//修改登录密码
    'codeUserModifyPasswordLimit' => 'user:user_modify_password_limit:mobile_',// 修改登录密码次数
    'codeUserForgetPassword' => 'user:user_forget_password:mobile_',// 忘记密码验证码
    'codeUserForgetPasswordLimit' => 'user:user_forget_password_limit:mobile_',// 忘记密码验证码限制次数
    'codeUserModifyPayPassword' => 'user:user_modify_pay_password:mobile_',//修改支付密码
    'codeUserModifyPayPasswordLimit' => 'user:user_modify_pay_password_limit:mobile_',// 修改支付密码次数
    'codeUserForgetPayPassword' => 'user:user_forget_pay_password:mobile_',//忘记支付密码
    'codeUserForgetPayPasswordLimit' => 'user:user_forget_pay_password_limit:mobile_',//次数
    'codeUserTrueNameAuth' => 'user:user_true_name_auth:mobile_',// 实名
    'codeUserTrueNameAuthLimit' => 'user:true_name_auth_limit:mobile_',//次数
    'longUrlToShortUrl' => 'long_url_to_short_url:',//长链接转短链接
    'personalAccountWithdraw' => 'personal_account_withdraw_user_id:',//个人提现并发访问锁
    'signIn' => 'sign_in:',//签到
];