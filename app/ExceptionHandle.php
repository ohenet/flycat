<?php
namespace app;

use app\handlers\ApiResponse;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\ClassNotFoundException;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\exception\RouteNotFoundException;
use think\exception\ValidateException;
use think\Response;
use Throwable;

/**
 * 应用异常处理类
 */
class ExceptionHandle extends Handle
{
    use ApiResponse;
    /**
     * 不需要记录信息（日志）的异常类列表
     * @var array
     */
    protected $ignoreReport = [
        HttpException::class,
        HttpResponseException::class,
        ModelNotFoundException::class,
        DataNotFoundException::class,
        ValidateException::class,
    ];

    /**
     * 记录异常信息（包括日志或者其它方式记录）
     *
     * @access public
     * @param  Throwable $exception
     * @return void
     */
    public function report(Throwable $exception): void
    {
        // 使用内置的方式记录异常日志
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request   $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response
    {
        // 添加自定义异常处理机制

        // 参数验证错误
        if ($e instanceof ValidateException) {
            return $this->responseError(422,(string)$e->getMessage());
        }

//        // 请求404异常
//        if (($e instanceof ClassNotFoundException || $e instanceof RouteNotFoundException) || ($e instanceof HttpException && $e->getStatusCode() == 404)) {
//            return $this->responseError($e->getCode() ?: 404,(string)$e->getMessage());
//        }
//
//        //请求500异常
//        if ($e instanceof \Exception ||  $e instanceof HttpException || $e instanceof \InvalidArgumentException || $e instanceof \ErrorException || $e instanceof \ParseError || $e instanceof \TypeError)  {
//            return $this->responseError($e->getCode() ?: 400,(string)$e->getMessage());
//        }


        // 其他错误交给系统处理
        return parent::render($request, $e);
    }



}
