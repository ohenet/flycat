<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\middleware;


use Closure;
use think\Request;
use think\Response;


class AllowCrossDomain
{
    /**
     * header头
     * @var array
     */
    protected $header = [
        'Access-Control-Allow-Origin'       => '*',
        'Access-Control-Allow-Headers'      => 'Authori-zation,Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With, Form-type',
        'Access-Control-Allow-Methods'      => 'GET,POST,PATCH,PUT,DELETE,OPTIONS,DELETE',
        'Access-Control-Max-Age'            =>  '1728000',
        'Access-Control-Allow-Credentials'  => 'true',
        'Access-Control-Expose-Headers'     => '*'
    ];
    /**
     * 允许跨域的域名
     * @var string
     */
    protected $cookieDomain;
    /**
     * 处理请求
     * @param Request $request
     * @param Closure $next
     * @return Response
     */
    public function handle($request, Closure $next)
    {
        $this->cookieDomain = config('cookie.domain');
        $header = $this->header;
        $origin = $request->header('origin');

        if ($origin && ('' != $this->cookieDomain && strpos($origin, $this->cookieDomain)))
            $header['Access-Control-Allow-Origin'] = $origin;

        if ($request->method(true) == 'OPTIONS') {
            $response = Response::create('ok')->code(200)->header($header);
        } else {
            $response = $next($request)->header($header);
        }
        $request->filter(['htmlspecialchars', 'strip_tags', 'addslashes', 'trim']);
        return $response;
    }
}