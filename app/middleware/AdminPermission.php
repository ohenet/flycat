<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\middleware;

class AdminPermission
{
    public function handle($request, \Closure $next)
    {
        if ($request->param('name') == 'think') {
            return redirect('index/think');
        }

        return $next($request);
    }
}