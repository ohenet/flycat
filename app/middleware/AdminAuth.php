<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\middleware;

use app\handlers\AdminResponse;
use utils\JwtAuth;
/**
 * 后台token验证
 * Class AdminAuth
 * @package app\middleware
 */
class AdminAuth
{
    use AdminResponse;
    public function handle($request, \Closure $next)
    {
        // 获取token值
        $token = trim(ltrim($request->header(env('app.token_header','Authorization')), 'Bearer'));
        // 判断是否获取成功
        if (!$token) return $this->responseError('请登录后访问',401);
        // 获取用户信息
        if (!(new JwtAuth())->verifyToken($token)) return $this->responseError('登录失效请重新登录',401);
        // 解析管理员信息
        $adminInfo = (new JwtAuth())->parseToken($token);
        if (!$adminInfo || !isset($adminInfo['id'])){
            return $this->responseError('登录失效请重新登录',401);
        }else{
            $request->adminInfo = $adminInfo;
        }

        return $next($request);
    }
}