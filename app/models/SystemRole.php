<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\models;

/**
 * 角色模型
 * Class SystemRole
 * @package app\models
 */
class SystemRole extends BaseModel
{
    //设置主键id
    protected $pk = 'id';

    // 设置数据表名称
    protected $name = 'system_role';

    // 自动写入时间戳
    protected $autoWriteTimestamp = true;


    // 写入时，将菜单id转成json格式
    public function setRulesAttr($value)
    {
        if (!is_string($value)){
            return implode(',', $value);
        }
        return $value;
    }

    // 读取时，将菜单id转为数组
    public function getRulesAttr($value)
    {
        return explode(',', $value);
    }
}