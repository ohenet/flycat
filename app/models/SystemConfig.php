<?php
/**
 * Created by 赵强.
 * Author 409072166@qq.com
 * Date: 2022-01-02
 * Time: 16:24
 */

namespace app\models;

class SystemConfig extends BaseModel
{
    //设置主键id
    protected $pk = 'id';

    // 设置当前模型对应的完整数据表名称
    protected $name = 'system_config';

    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
}