<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\models;

/**
 * 后台用户模型
 * Class SystemAdmin
 * @package app\models
 */
class SystemAdmin extends BaseModel
{
    //设置主键id
    protected $pk = 'id';

    // 设置当前模型对应的完整数据表名称
    protected $name = 'system_admin';

    // 自动写入时间戳
    protected $autoWriteTimestamp = true;


}