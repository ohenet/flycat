<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\models;
use think\Model;

/**
 * 模型基类
 * Class BaseModel
 * @package app\models
 */
class BaseModel extends Model
{
    // 错误信息
    protected $error = '';


}