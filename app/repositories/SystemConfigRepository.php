<?php
/**
 * Created by 赵强.
 * Author 409072166@qq.com
 * Date: 2022-01-02
 * Time: 22:26
 */

namespace app\repositories;

use app\models\SystemConfig;

/**
 * 系统配置仓库
 * Class SystemConfigRepository
 * @package app\repositories
 */
class SystemConfigRepository
{
    public function create(array $param)
    {
        return SystemConfig::create($param);
    }

    public function byId($id)
    {
        return SystemConfig::find($id);
    }

    public function byName($name)
    {
        return SystemConfig::where('name',$name)->find();
    }

    /**
     * 获取配置值
     * @param $name
     * @return mixed
     */
    public function getValue($name)
    {
        return SystemConfig::where('name',$name)->value('value');
    }

    /**
     * 获取分组配置
     * @param int $group_id
     * @return SystemConfig[]|array|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getGroupConfigList($group_id = 1)
    {
        return SystemConfig::where('group_id', $group_id)
            ->order('sort desc,id asc')
            ->select();
    }

    /**
     * 获取配置列表
     * @param array $where
     * @param int $page
     * @param int $limit
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     */
    public function getList($where = [], $limit = 15)
    {
        return SystemConfig::where($where)
            ->order('sort desc,id asc')
            ->paginate($limit);
    }

}