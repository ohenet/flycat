<?php
/**
 * Created by 赵强.
 * Author 409072166@qq.com
 * Date: 2022-01-02
 * Time: 16:24
 */

namespace app\repositories;

use app\models\SystemConfigGroup;

/**
 * 配置分组
 * Class SystemConfigGroupRepository
 * @package app\repositories
 */
class SystemConfigGroupRepository
{

    /**
     * @param array $param
     */
    public function create(array $param)
    {
        SystemConfigGroup::create($param);
    }

    /**
     * @param $id
     * @return SystemConfigGroup|array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function byId($id)
    {
        return SystemConfigGroup::find($id);
    }

    /**
     * 获取所有配置分组
     * @param $where
     * @param $field
     */
    public function getAll($where, $field = [])
    {
        return SystemConfigGroup::where($where)
            ->where('status',1)
            ->field($field)
            ->order('sort desc,id asc')
            ->select();
    }

    /**
     * 获取配置分组列表
     * @param $where
     * @param $page
     * @param $limit
     * @return SystemConfigGroup[]|array|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList($where = [], $page = 1, $limit = 15)
    {
        return SystemConfigGroup::where($where)
            ->order('sort desc,id asc')
            ->page($page,$limit)
            ->select();
    }
}