<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\repositories;

use app\models\SystemRole;

class SystemRoleRepository
{
    /**
     * 创建角色
     * @param array $params
     * @return SystemRole|\think\Model
     */
    public function create(array $params)
    {
        return SystemRole::create($params);
    }

    /**
     * 根据ID查找角色
     * @param $id
     * @return SystemRole|array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function byId($id)
    {
        return SystemRole::find($id);
    }

    /**
     * 是否可登录后台
     * @param $id
     * @return SystemRole|array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function isAccess($id)
    {
        return SystemRole::where([
            'id'=>$id,
            'status'=>1
        ])->find();
    }








}