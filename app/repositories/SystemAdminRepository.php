<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\repositories;

use app\models\SystemAdmin;

class SystemAdminRepository
{

    /**
     *创建一个用户
     * @param array $param
     */
    public function create(array $param)
    {
        SystemAdmin::create($param);
    }

    /**
     * 根据用户id查找
     * @param $id
     * @return SystemAdmin|array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function byId($id)
    {
        return SystemAdmin::find($id);
    }

    /**
     * 根据用户名查找用户
     * @param $username
     * @return SystemAdmin|array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function byUsername($username)
    {
        return SystemAdmin::where('username',$username)->find();
    }

    /**
     * 更加邮箱查找用户
     * @param $email
     * @return SystemAdmin|array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function byEmail($email)
    {
        return SystemAdmin::where('email',$email)->find();
    }

    /**
     * 更新用户密码
     * @param $id
     * @param $password
     * @return SystemAdmin
     */
    public function updatePassword($id,$password)
    {
        return SystemAdmin::where('id',$id)->update([
            'password'=>$password
        ]);
    }



    /**
     * 更新登录信息
     * @param $id
     * @param $param
     * @return SystemAdmin
     */
    public function updateLoginInfo($id,$param)
    {
        return SystemAdmin::where('id',$id)->update($param);
    }


}