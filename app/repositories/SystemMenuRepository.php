<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\repositories;

use app\models\SystemMenu;

/**
 * 后台菜单数据仓库
 * Class SystemMenuRepository
 * @package app\repositories
 */
class SystemMenuRepository
{
    /**
     * 创建
     * @param array $param
     */
    public function create(array $param)
    {
        return SystemMenu::create($param);
    }

    /**
     * 根据id获取菜单列表
     * @param array $where
     * @param array $ids
     * @return SystemMenu[]|array|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function byIdsMenus($ids = [],$where = [])
    {
        return SystemMenu::where($where)->order('sort desc')->select($ids)->toArray();
    }

    /**
     * 获取菜单
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getMenu($where = [])
    {
        return SystemMenu::where($where)->order('sort desc')->select()->toArray();
    }

}