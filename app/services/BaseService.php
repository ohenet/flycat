<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\services;

use app\handlers\AdminResponse;
use service\RedisService;

/**
 * 基础服务层
 * Class BaseService
 * @package app\services
 */
class BaseService
{
    // 导入Response类
    use AdminResponse;
    // 导入Redis类
    use RedisService;


    /**
     * 检查短信验证码
     * @param $mobile
     * @param $code
     * @param $type
     */
    public function checkCode($mobile, $code, $type)
    {
        $redisKey = "";
        // 注册
        if ('register' == $type) {
            $redisKey = RedisService::redisKeyCodeUserRegister() . $mobile;
        }
        // 忘记密码
        if ('forget_password' == $type) {
            $redisKey = RedisService::redisKeyCodeUserForgetPassword() . $mobile;
        }
        // 忘记支付密码
        if ('forget_pay_password' == $type) {
            $redisKey = RedisService::redisKeyCodeUserForgetPayPassword() . $mobile;
        }
        // 实名
        if ('truename_auth' == $type) {
            $redisKey = RedisService::redisKeyCodeUserTrueNameAuth() . $mobile;
        }
        // 修改支付密码
        if ('modify_pay_password' == $type) {
            $redisKey = RedisService::redisKeyCodeUserModifyPayPassword() . $mobile;
        }
        // 修改登录密码
        if ('modify_password' == $type) {
            $redisKey = RedisService::redisKeyCodeUserModifyPassword() . $mobile;
        }
        // 验证码登录
        if ('verify_login' == $type) {
            $redisKey = RedisService::redisKeyCodeVerifyLogin() . $mobile;
        }

        $redisCode = RedisService::redis()->get($redisKey);
        if ($code != $redisCode) {
            return false;
        } else {
            //删除验证码
            RedisService::redis()->delete($redisKey);
            return true;
        }


    }

    /**
     * 获取筛选条件
     * @return array
     */
    public function getMap()
    {
        $map = [];
        // 常规查询条件
        $param = request()->param();
        if ($param) {

            // 筛选名称
            if (isset($param['name']) && $param['name']) {
                $map[] = ['name', 'like', "%{$param['name']}%"];
            }
            // 筛选昵称
            if (isset($param['username']) && $param['username']) {
                $map[] = ['username', 'like', "%{$param['username']}%"];
            }

            // 筛选标题
            if (isset($param['title']) && $param['title']) {
                $map[] = ['title', 'like', "%{$param['title']}%"];
            }

            // 筛选类型
            if (isset($param['type']) && $param['type']) {
                $map[] = ['type', '=', $param['type']];
            }

            // 筛选状态
            if (isset($param['status']) && $param['status']) {
                $map[] = ['status', '=', $param['status']];
            }

            // 邮箱
            if (isset($param['email']) && $param['email']) {
//                $map[] = ['email', '=', $param['email']];
                $map[] = ['email', 'like', "%{$param['email']}%"];
            }

            // 手机号码
            if (isset($param['mobile']) && $param['mobile']) {
//                $map[] = ['mobile', '=', $param['mobile']];
                $map[] = ['mobile', 'like', "%{$param['mobile']}%"];
            }

        }

        return $map;
    }

    public function getPage()
    {

    }
}