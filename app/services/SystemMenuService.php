<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\services;

use app\repositories\SystemMenuRepository;
use app\repositories\SystemRoleRepository;

/**
 * 后台菜单
 * Class SystemMenuService
 * @package app\services
 */
class SystemMenuService extends BaseService
{
    protected $systemMenuRepository;
    protected $systemRoleRepository;
    public function __construct(SystemMenuRepository $systemMenuRepository,
                                SystemRoleRepository $systemRoleRepository)
    {
        $this->systemMenuRepository = $systemMenuRepository;
        $this->systemRoleRepository = $systemRoleRepository;
    }

    public function getAuthMenus($role_id)
    {
        $role = $this->systemRoleRepository->byId($role_id);
        // 如果是超级管理员就获取全部节点
        if ($role->id === 1){
            return $this->systemMenuRepository->getMenu($role->rules);
        }else{
            // 普通管理员获取相应节点
            return false;
        }
    }

    public function cutPermission($menu)
    {

    }


}