<?php
/**
 * Created by 赵强.
 * Author 409072166@qq.com
 * Date: 2022-01-02
 * Time: 16:35
 */

namespace app\services;

use app\repositories\SystemConfigRepository;

/**
 * 系统配置服务类
 * Class SystemConfigService
 * @package app\services
 */
class SystemConfigService extends BaseService
{
    protected $systemConfigRepository;

    public function __construct(SystemConfigRepository $systemConfigRepository)
    {
        $this->systemConfigRepository = $systemConfigRepository;
    }

    /**
     * 获取配置列表
     * @param int $group_id
     * @return \think\Response|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList($group_id = 0)
    {
        $map = $this->getMap();
        if ($group_id){
            $map[] = ['group_id',$group_id];
        }
        return $this->responseSuccess($this->systemConfigRepository->getList($map));
    }
}