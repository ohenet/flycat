<?php
/**
 * Created by 赵强.
 * Author 409072166@qq.com
 * Date: 2022-01-02
 * Time: 16:35
 */

namespace app\services;

use app\repositories\SystemConfigGroupRepository;

/**
 * 系统配置分组服务类
 * Class SystemConfigGroupService
 * @package app\services
 */
class SystemConfigGroupService extends BaseService
{
    protected $systemConfigGroupRepository;

    public function __construct(SystemConfigGroupRepository $systemConfigGroupRepository)
    {
        $this->systemConfigGroupRepository = $systemConfigGroupRepository;
    }

    /**
     * 获取tabs
     * @return \think\Response|\think\response\Json
     */
    public function getTabs()
    {
        return $this->responseSuccess($this->systemConfigGroupRepository->getAll(['status'=>1]));
    }

    /**
     * 获取全部配置分组
     * @return \think\Response|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList()
    {
        return $this->responseSuccess($this->systemConfigGroupRepository->getList());
    }

}