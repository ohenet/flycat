<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\services;

use app\repositories\SystemMenuRepository;
use app\repositories\SystemRoleRepository;

/**
 * 角色
 * Class SystemRoleService
 * @package app\services
 */
class SystemRoleService extends BaseService
{
    protected $systemRoleRepository;
    protected $systemMenuRepository;

    public function __construct(SystemRoleRepository $systemRoleRepository,
                                SystemMenuRepository $systemMenuRepository)
    {
        $this->systemMenuRepository = $systemMenuRepository;
        $this->systemRoleRepository = $systemMenuRepository;
    }


    public function getRoles($role_id)
    {
        $role = $this->systemRoleRepository->byId($role_id);
        // 如果是超级管理员就获取全部节点
        if ($role->id === 1){
            return $this->systemMenuRepository->getMenu($role->rules);
        }else{
            // 普通管理员获取相应节点
            return $this->systemMenuRepository->getMenu();
        }
    }




    public function builderRules()
    {

    }
}