<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\services;

use app\repositories\SystemAdminRepository;
use app\repositories\SystemMenuRepository;
use app\repositories\SystemRoleRepository;
use utils\JwtAuth;

/**
 * 后台授权服务
 * Class AuthService
 * @package app\services
 */
class AuthService extends BaseService
{

    protected $systemAdminRepository;
    protected $systemRoleRepository;
    protected $systemMenuRepository;

    public function __construct(SystemAdminRepository $systemAdminRepository,
                                SystemRoleRepository $systemRoleRepository,SystemMenuRepository $systemMenuRepository)
    {
        $this->systemAdminRepository = $systemAdminRepository;
        $this->systemRoleRepository = $systemRoleRepository;
        $this->systemMenuRepository = $systemMenuRepository;
    }

    /**
     * 密码登录
     * @param string $username
     * @param string $password
     * @return \app\models\SystemAdmin|array|\think\Model|\think\Response|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function authUserToLogin(string $username, string $password)
    {
        // 查询用户
        $adminInfo = $this->systemAdminRepository->byUsername($username);
        // 判断账户是否存在
        if (!$adminInfo) {
            return $this->responseError('管理员不存在');
        }
        // 验证密码
        if (!password_verify($password,$adminInfo->password)){
            return $this->responseError('用户名或密码错误');
        }
        // 角色验证
        if (!$adminInfo->role||!$adminInfo->status) {
            return $this->responseError('禁止访问或未分配角色');
        }
        // 权限验证
        if (!$this->systemRoleRepository->isAccess($adminInfo->role)) {
            return $this->responseError('用户所在角色未启用或禁止访问后台！');
        }
        // 更新登录信息
        $adminInfo->update([
            'id'=>$adminInfo->id,
            'last_login_time'=>request()->time(),
            'last_login_ip'=>request()->ip(),
        ]);

        // 组装数据
        $result['token_type'] = env('app.token_prefix','Bearer');
        $result['token_expire'] = (int)env('app.token_expire',7200);
        $result['access_token'] = $this->makeToken($adminInfo->toArray());
        $result['user'] = $adminInfo;

        return $this->responseSuccess('登录成功',$result);
    }

    /**
     * 扫码登录
     * @param $code
     * @return \think\Response|\think\response\Json
     */
    public function scanCodeToLogin($code)
    {
        return $this->responseJson(0,'登录成功');
    }


    /**
     * 推出登录
     * @param $token
     * @return \think\Response|\think\response\Json
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function logout($token)
    {
        (new JwtAuth())->deleteToken($token);
        return $this->responseJson(0,'退出登录成功');
    }

    public function getInfo($admin_id)
    {
        $adminInfo = $this->systemAdminRepository->byId($admin_id);
        $result['user'] = $adminInfo;
        // 获取角色权限
        $result['role'] = $this->getRoles($adminInfo->role);

        return $this->responseSuccess($result);
    }

    public function getAuthMenus($role_id)
    {
        $role = $this->systemRoleRepository->byId($role_id);
        if ($role->id == 1){
            // 超级管理员获取全部菜单
            $menus = $this->systemMenuRepository->getMenu(['type'=>1]);
        }else{
            // 普通管理员获取相应节点
            $menus = $this->systemMenuRepository->byIdsMenus($role->rules,['type'=>1]);
        }

        return $this->responseSuccess($menus);
    }

    /**
     * 创建token
     * @param $admininfo
     * @return string
     */
    public function makeToken($admininfo)
    {
        return (new JwtAuth())->createToken($admininfo,'admin');
    }

    /**
     * 获取权限列表
     * @param $role_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getRoles($role_id)
    {
        $role = $this->systemRoleRepository->byId($role_id);
        if ($role->id == 1){
            // 超级管理员获取全部菜单
            $menus = $this->systemMenuRepository->getMenu();
        }else{
            // 普通管理员获取相应节点
            $menus = $this->systemMenuRepository->byIdsMenus($role->rules);
        }
        $role->permissions = $this->builderRules($menus);

        return [
            'id' => $role->id,
            'name' => $role->name,
            'describe' => $role->description,
            'permissions' => $this->builderRules($menus),
        ];;
    }

    /**
     * 构建权限
     * @param $menus
     * @param int $pid
     * @return array
     */
    public function builderRules($menus,$pid = 0)
    {
        $rules = [];
        $rules = array_values($rules);
        foreach ($menus as $key => $value){
            // 判断type=1的是菜单
            if ($value['type'] == 1){
                $rules[] = [
                    // 菜单唯一标示
                    'permissionId' => $value['path'],
                    // 菜单名称
                    'permissionName' => $value['title'],
                    // 按钮或方法权限
                    'actionEntitySet' => $this->builderAction($menus,$value['id'])
                ];
            }
        }
        return $rules;
    }

    /**
     * 构建权限详情
     * @param $menus
     * @param int $pid
     * @return array
     */
    public function builderAction($menus,$pid = 0)
    {
        $actionEntitySet = [];
        foreach ($menus as $key => $value) {
            // 判断类型为type=2是按钮或者方法
            if ($value['pid'] == $pid && $value['type'] == 2){
                $actionEntitySet[] = [
                    'describe' => $value['title'],
                    'action' => $value['action'],
                ];
            }
        }
        return $actionEntitySet;
    }

}