<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */
namespace app\handlers;

use think\Response;

trait ApiResponse
{
    /**
     * 全局状态码
     * @var string[]
     */
    public $statusTexts = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',            // RFC2518
        103 => 'Early Hints',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',          // RFC4918
        208 => 'Already Reported',      // RFC5842
        226 => 'IM Used',               // RFC3229
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',    // RFC7238
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable|',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',                                               // RFC2324
        421 => 'Misdirected Request',                                         // RFC7540
        422 => 'Unprocessable Entity',                                        // RFC4918
        423 => 'Locked',                                                      // RFC4918
        424 => 'Failed Dependency',                                           // RFC4918
        425 => 'Too Early',                                                   // RFC-ietf-httpbis-replay-04
        426 => 'Upgrade Required',                                            // RFC2817
        428 => 'Precondition Required',                                       // RFC6585
        429 => 'Too Many Requests',                                           // RFC6585
        431 => 'Request Header Fields Too Large',                             // RFC6585
        451 => 'Unavailable For Legal Reasons',                               // RFC7725
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',                                     // RFC2295
        507 => 'Insufficient Storage',                                        // RFC4918
        508 => 'Loop Detected',                                               // RFC5842
        510 => 'Not Extended',                                                // RFC2774
        511 => 'Network Authentication Required',                             // RFC6585
        520 => '服务器维护升级中，稍等片刻',
        0   => 'success',
        // 1 开始用户相关状态码
        10100 => '用户名或密码不正确',
        10101 => '手机号码已被占用',
        10102 => '手机号码不存在',
        10103 => '原登录密码不正确',
        10104 => '原支付密码不正确',
        10105 => '账号被冻结',
        10106 => '请完成实名认证',
        10109 => '发送短信次数超出限制',
        10110 => '短信验证码不正确',
        10111 => '邀请码不存在',
        10112 => '支付密码不正确',
        10113 => '手机号码未绑定该账号',
        10114 => '身份证号码与账号信息不匹配',
        10115 => '支付密码已设置无需重复设置',
        10116 => '微信登录失败',
        10117 => '无访问权限',
        10118 => '该手机号码未注册',
        10119 => '请输入邀请码或邀请人手机号',
        10120 => '微信授权信息为空',

        10200 => '无效文件',
        10201 => '图片格式不支持',
        10202 => '图片大小超出限制',
        10203 => '图片上传失败',
        10204 => '图片不存在',
        10205 => '图片参数格式不正确',
        10206 => '分享链接已失效',
        10300 => '您已通过实名认证',
        10303 => '请先上传身份照',

        10701 => '提现失败',
        10702 => '余额不足',
        10900 => '您的信息有误',
        10901 => '暂不支持该银行',

        // 2 商城相关错误码
        20000 => '商品不存在',
        20001 => '该商品已下架',
        20002 => '商品数量必须大于0',
        20003 => '购物车最多只能放20种商品',
        20004 => '该商品必须传递商品规格',
        20005 => '商品规格不存在',
        20006 => '加入购物车失败',
        20007 => '购物车不存在该商品',
        20008 => '购买至少1件',
        20009 => '购买数量不得大于库存',
        20010 => '购物车无选中商品',
        20011 => '最多添加20个收货地址',
        20012 => '收货地址格式不正确',
        20013 => '收货地址不存在',
        20014 => '运费模板区域尚未配置',
        20015 => '运费模板尚未配置',
        20016 => '运费模板尚未开启',
        20017 => '该商品收藏不存在',
        20018 => '商品规格价格不存在',
        20019 => '订单不存在',
        20020 => '该商品收藏已存在',
        20021 => '订单商品不存在',
        20022 => '您已经评价过该商品',
        20023 => '物流星级0-5',
        20024 => '商品评价0-5',
        20025 => '服务星级0-5',
        20026 => '该订单暂不允许确认收货',
        20027 => '请选择支付方式',
        20028 => '支付方式与下单选择不一致',
        20029 => '该订单已支付，不允许重复支付',
        20030 => '该订单不可评价',
        20031 => '订单不存在该商品',
        20032 => '已取消的订单不允许申请售后',
        20033 => '未发货的订单不允许申请售后',
        20034 => '未支付的订单不允许申请售后',
        20035 => '该商品已申请售后，请耐心等待',
        20036 => '退换货数量不能大于下单商品数量',
        20037 => '该订单商品申请售后不存在',
        20038 => '该商品只允许点击立即购买',
        20039 => '该商品只能购买1个',
        20040 => '您已购买过该商品',
        20041 => '定制商品不允许退货退款',
        20050 => '该商品只限普通用户购买',

        // 3 营销相关状态码
        30000 => ''


    );

    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var string
     */
    protected $statusText;

    /**
     * 返回json数据
     * @param $code
     * @param array $data
     * @param bool $isDefault
     * @param string $message
     * @param string $msg
     * @return \think\response\Json
     */
    public function responseJson($code, $data = [], $msg = ''): Response
    {
        $this->statusCode = (int)$code;
        if (empty($msg)){
            $msg = isset($this->statusTexts[$code]) ? $this->statusTexts[$code] : 'unknown status';
        }
        $result = [
            'code' => (int)$code,
            'msg' => $msg
        ];
        $json = empty($data) ? $result : array_merge($result, ['data' => $data]);
        return json($json);
    }

    /**
     * 返回成功数据
     * @param array $data
     * @param bool $isDefault
     * @param string $message
     * @param string $msg
     * @return \think\response\Json
     */
    public function responseSuccess($msg = '',$data = [] ): Response
    {
        if (is_array($msg)||is_object($msg)) {
            $data = $msg;
            $msg = '';
        }
        return $this->responseJson(0, $data, $msg);
    }

    /**
     * 返回失败数据
     * @param $code
     * @param array $data
     * @param bool $isDefault
     * @param string $message
     * @param string $msg
     * @return \think\response\Json
     */
    public function responseError($code, $msg = '',$data = []): Response
    {
        return $this->responseJson($code, $data, $msg);
    }
}