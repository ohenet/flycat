<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\admin\validate;

use think\Validate;

class SystemAdmin extends Validate
{
    // 定义验证规则
    protected $rule = [
        'username|用户名' => 'require|alphaNum',
        'nickname|昵称'  => 'require',
        'role|角色'      => 'require',
        'email|邮箱'     => 'email',
        'password|密码'  => 'require|length:6,20',
        'captcha|验证码' => 'require',
    ];

    // 定义验证提示
    protected $message = [
        'username.require' => '请输入用户名',
        'email.require'    => '邮箱不能为空',
        'email.email'      => '邮箱格式不正确',
        'password.require' => '密码不能为空',
        'password.length'  => '密码长度6-20位',
        'captcha.require'    => '验证码不能为空',
    ];

    // 定义验证场景
    protected $scene = [
        //更新
        'update'  =>  ['email', 'password' => 'length:6,20', 'mobile', 'role', '__token__'],
        //登录
        'login'  =>  ['username', 'password'],
    ];
}