<?php
/**
 * Created by 赵强.
 * Author 409072166@qq.com
 * Date: 2022-01-01
 * Time: 0:49
 */

use think\facade\Route;
// 不需要权限验证
Route::group(function (){
    //账号密码登录
    Route::post('login', 'Login/login');
    Route::post('captcha', 'Login/captcha');
    Route::post('logout', 'Login/logout');
    // 权限相关
    Route::group('auth',function (){
        Route::get('info', 'Auth/info');
        Route::get('menus', 'Auth/menus');
    })->middleware(\app\middleware\AdminAuth::class);

    //系统相关
    Route::group('system',function (){
        // 获取配置分组
        Route::get('config/group', 'SystemConfigGroup/menus');
        // 获取配置列表
        Route::get('config/list', 'SystemConfig/list');

    })->middleware(\app\middleware\AdminAuth::class);


})->middleware(\app\middleware\AllowCrossDomain::class);

