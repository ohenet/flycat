<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\BaseController;
use app\services\AuthService;
use app\services\SystemMenuService;
use service\RedisService;
use think\App;
use utils\JwtAuth;

/**
 * 后台首页控制器
 * Class Index
 * @package app\admin\controller
 */
class Index extends BaseController
{
    use RedisService;
    protected $systemMenuService;
    protected $authService;
    public function __construct(App $app,SystemMenuService $systemMenuService,AuthService $authService)
    {
        parent::__construct($app);
        $this->systemMenuService = $systemMenuService;
        $this->authService = $authService;
    }

    public function index()
    {

    }
}
