<?php
/**
 * Created by 赵强.
 * Author 409072166@qq.com
 * Date: 2022-01-01
 * Time: 1:51
 */

namespace app\admin\controller;

use app\BaseController;
use app\services\AuthService;
use think\App;

class Login extends BaseController
{
    protected $authService;

    public function __construct(App $app,AuthService $authService)
    {
        parent::__construct($app);
        $this->authService = $authService;
    }

    /**
     * 账户密码登录
     * @return \think\Response|\think\response\Json|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function login()
    {
        $data = $this->request->post();

        $this->validate($data, 'SystemAdmin.login');

        // 验证码验证
        // if(!captcha_check($data['captcha'])) return $this->responseError('验证码错误');

        // 返回登录信息
        return $this->authService->authUserToLogin($data['username'], $data['password']);
    }

    /**
     * 退出登录
     * @return \think\Response|\think\response\Json
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function logout()
    {
        // 获取token
        $token = trim(ltrim($this->request->header(env('app.token_header','Authorization')), 'Bearer'));
        // 退出登录
        return $this->authService->logout($token);
    }

    /**
     * 登录验证码
     * @return \think\Response
     */
    public function captcha()
    {
        return captcha();
    }
}