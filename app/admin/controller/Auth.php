<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\admin\controller;

use app\services\AuthService;
use think\App;

/**
 * 后台授权控制器
 * Class Auth
 * @package app\admin\controller
 */
class Auth extends AdminController
{
    protected $authService;

    public function __construct(App $app,AuthService $authService)
    {
        parent::__construct($app);
        $this->authService = $authService;
    }

    /**
     * 管理员授权信息
     * @return \think\Response|\think\response\Json
     */
    public function info()
    {
        return $this->authService->getInfo($this->adminInfo['id']);
    }

    public function menus()
    {
        return $this->authService->getAuthMenus($this->adminInfo['role']);
    }



}