<?php
/**
 * Created by 赵强.
 * Author 409072166@qq.com
 * Date: 2022-01-02
 * Time: 16:23
 */

namespace app\admin\controller;

use app\services\SystemConfigGroupService;
use think\App;

/**
 * 配置分组
 * Class SystemConfigGroup
 * @package app\admin\controller
 */
class SystemConfigGroup extends AdminController
{

    protected $systemConfigGroupService;
    public function __construct(App $app,SystemConfigGroupService $systemConfigGroupService)
    {
        parent::__construct($app);
        $this->systemConfigGroupService = $systemConfigGroupService;
    }

    /**
     * 获取配置分组tab
     * @return \think\Response|\think\response\Json
     */
    public function tabs()
    {
        return $this->systemConfigGroupService->getTabs();
    }


    /**
     * 获取分组列表
     * @return \think\Response|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list()
    {
        return $this->systemConfigGroupService->getList();
    }
}