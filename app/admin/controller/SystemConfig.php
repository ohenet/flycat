<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace app\admin\controller;

use app\services\SystemConfigService;
use think\App;

/**
 * 配置管理
 * Class SystemConfig
 * @package app\admin\controller
 */
class SystemConfig extends AdminController
{
    protected $systemConfigService;

    public function __construct(App $app,SystemConfigService $systemConfigService)
    {
        parent::__construct($app);
        $this->systemConfigService = $systemConfigService;
    }

    /**
     * 获取列表
     * @return \think\Response|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list()
    {
        $group_id = $this->request->param('group_id');

        return $this->systemConfigService->getList($group_id);
    }


    public function add()
    {
        // 获取数据
        $data = $this->request->post();
        // 验证数据
        $this->validate($data, 'SystemConfig');

//        return $this->systemConfigService->c
    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }

    public function status()
    {

    }
}