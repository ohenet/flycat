<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace service;

use think\facade\Cache;

/**
 * Redis操作类
 * Class RedisService
 * @package service
 */
trait RedisService
{
    public static $expire = 3600; //默认存储时间（秒）

    // 实例化redis缓存
    public static function redis()
    {
        return Cache::store('redis');
    }

    // 对应的key是什么，可以到config->redisnamespace.php文件查看说明

    /**
     * @return mixed
     */
    public static function redisKeyUserInfo()
    {
        return config('redisnamespace.userInfo');
    }

    /**
     * @return mixed
     */
    public static function redisKeyUserToken()
    {
        return config('redisnamespace.userToken');
    }

    public static function redisKeyCodeUserRegister()
    {
        return config('redisnamespace.codeUserRegister');
    }

    public static function redisKeyCodeUserRegisterLimit()
    {
        return config('redisnamespace.codeUserRegisterLimit');
    }

    public static function redisKeyCodeUserForgetPassword()
    {
        return config('redisnamespace.codeUserForgetPassword');
    }

    public static function redisKeyCodeUserForgetPasswordLimit()
    {
        return config('redisnamespace.codeUserForgetPasswordLimit');
    }

    public static function redisKeyCodeUserModifyPayPassword()
    {
        return config('redisnamespace.codeUserModifyPayPassword');
    }


    public static function redisKeyCodeUserModifyPayPasswordLimit()
    {
        return config('redisnamespace.codeUserModifyPayPasswordLimit');
    }

    public static function redisKeyCodeUserModifyPassword()
    {
        return config('redisnamespace.codeUserModifyPassword');
    }

    public static function redisKeyCodeUserModifyPasswordLimit()
    {
        return config('redisnamespace.codeUserModifyPasswordLimit');
    }

    public static function redisKeyCodeUserForgetPayPassword()
    {
        return config('redisnamespace.codeUserForgetPayPassword');
    }

    public static function redisKeyCodeUserForgetPayPasswordLimit()
    {
        return config('redisnamespace.codeUserForgetPayPasswordLimit');
    }

    public static function redisKeyCodeUserTrueNameAuth()
    {
        return config('redisnamespace.codeUserTrueNameAuth');
    }

    public static function redisKeyCodeVerifyLogin()
    {
        return config('redisnamespace.codeVerifyLogin');
    }

    public function redisKeyLongUrlToShortUrl()
    {
        return config('redisnamespace.longUrlToShortUrl');
    }

    // 签到
    public function redisKeySignIn()
    {
        return config('redisnamespace.signIn');
    }

}
