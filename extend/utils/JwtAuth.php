<?php
/**
 * Created By 赵强
 * Author 409072166@qq.com
 */

namespace utils;

use Firebase\JWT\JWT;
use service\RedisService;

class JwtAuth
{
    /**
     * token加密密钥
     * @var string
     */
    private $secret = '';

    /**
     * token过期时间
     * @var string
     */
    private $token_expire = '';

    public function __construct()
    {
        $this->setSecret(env('app.token_secret','flycat')); //jwt的签发密钥
        $this->setTokenExpire(env('app.token_expire',7200));
    }

    /**
     * 获取过期时间
     * @return string
     */
    public function getTokenExpire(): string
    {
        return $this->token_expire;
    }

    /**
     * 设置过期时间
     * @param string $token_expire
     */
    public function setTokenExpire(string $token_expire): void
    {
        $this->token_expire = $token_expire;
    }

    /**
     * 获取密钥
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * 设置密钥
     * @param string $secret
     */
    public function setSecret(string $secret): void
    {
        $this->secret = $secret;
    }

    /**
     * 解析token
     * @param $jwt
     * @return array
     */
    public function parseToken($jwt)
    {
        $info = JWT::decode($jwt,$this->getSecret(),array('HS256'));
        return (array)$info->data;
    }

    /**
     * 验证token是否过期
     * @param $token
     * @param $type
     * @return bool
     */
    public function verifyToken($token):bool
    {
        $jwt = RedisService::redis()->get(RedisService::redisKeyUserToken().md5($token));
        return !is_null($jwt);
    }


    /**
     * 创建token
     * @param array $data
     * @param string $type
     * @param int $expireTime
     * @param string $alg
     * @return string
     */
    public function createToken(array $data = [],string $type = '',$expireTime = 0,$alg = 'HS256'):string
    {
        $time = time(); //签发时间
        $expireTime = $expireTime !== 0 ? $expireTime : $this->getTokenExpire(); // 多少秒后过期
        $expire = $time + $expireTime; //过期时间
        $params = array(
            "iss" => app()->request->host(),//签发组织
            "aud" => app()->request->host(), //签发作者
            "iat" => $time, // 签发时间
            "nbf" => $time, // 生效时间
            "exp" => $expire, // 过期时间
            "data" => $data, // 用户数据
        );
        $jwt = JWT::encode($params, $this->getSecret(),$alg);
        // 缓存token
        RedisService::redis()->tag($type)->set(RedisService::redisKeyUserToken().md5($jwt),$data,$expireTime);
        return $jwt;
    }


    /**
     * 删除一个token
     * @param $token
     * @return bool
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function deleteToken($token):bool
    {
        return RedisService::redis()->delete(RedisService::redisKeyUserToken().md5($token));
    }
}