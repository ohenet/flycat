/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : flycat

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-12-24 17:18:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for flycat_system_admin
-- ----------------------------
DROP TABLE IF EXISTS `flycat_system_admin`;
CREATE TABLE `flycat_system_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(32) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(96) NOT NULL DEFAULT '' COMMENT '密码',
  `email` varchar(64) NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `avatar` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '头像',
  `role` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '主角色ID',
  `roles` varchar(255) NOT NULL DEFAULT '' COMMENT '副角色ID',
  `group` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '部门id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `last_login_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后一次登录时间',
  `last_login_ip` varchar(16) NOT NULL DEFAULT '0' COMMENT '登录ip',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态：0禁用，1启用',
  PRIMARY KEY (`id`),
  KEY `username` (`username`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='后台管理员表';

-- ----------------------------
-- Records of flycat_system_admin
-- ----------------------------
INSERT INTO `flycat_system_admin` VALUES ('1', 'admin', 'admin', '$2y$10$woGKaxFfu6HfT4X9t8JYWe5KwWVWLke1C9qk8WwI9ydeszkKWJ.EK', '', '', '0', '1', '', '0', '0', '1640336403', '1640336402', '127.0.0.1', '100', '1');

-- ----------------------------
-- Table structure for flycat_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `flycat_system_menu`;
CREATE TABLE `flycat_system_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单id',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '菜单标题',
  `icon` varchar(64) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `controller` varchar(64) NOT NULL COMMENT '控制器',
  `action` varchar(32) NOT NULL COMMENT '方法',
  `router` varchar(64) NOT NULL DEFAULT '' COMMENT '路由名称',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `type` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '类型：1：菜单 2：按钮/功能',
  `params` varchar(255) NOT NULL DEFAULT '' COMMENT '参数',
  `target` varchar(64) NOT NULL DEFAULT '' COMMENT '打开方式',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';

-- ----------------------------
-- Records of flycat_system_menu
-- ----------------------------
INSERT INTO `flycat_system_menu` VALUES ('1', '0', '首页', '', '', '', 'home', '/home', '100', '1', '', 'self', '0', '1632894883', '1');
INSERT INTO `flycat_system_menu` VALUES ('2', '0', '系统', '', '', '', 'system', '/system', '100', '1', '', 'self', '0', '1632899191', '1');
INSERT INTO `flycat_system_menu` VALUES ('3', '2', '系统菜单', '', '', '', 'menu', '/system/menu', '100', '1', '', 'self', '0', '1632899188', '1');
INSERT INTO `flycat_system_menu` VALUES ('4', '2', '用户管理', '', '', '', 'admin', '/system/admin', '100', '1', '', 'self', '0', '1632895241', '1');
INSERT INTO `flycat_system_menu` VALUES ('5', '2', '角色管理', '', '', '', 'role', '/system/role', '100', '1', '', 'self', '0', '1632898364', '1');
INSERT INTO `flycat_system_menu` VALUES ('6', '2', '附件管理', '', '', '', 'attachment', '/attachment', '100', '0', '', 'self', '0', '1632992039', '1');
INSERT INTO `flycat_system_menu` VALUES ('7', '0', '日志管理', '', '', '', 'log', '/log', '100', '0', '', 'self', '0', '1633917398', '1');
INSERT INTO `flycat_system_menu` VALUES ('8', '7', '登录日志', '', '', '', 'login_log', '/log/login_log', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('9', '7', '系统日志', '', '', '', 'system_log', '/log/system_log', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('10', '7', '秉坤日志', '', '', '', 'pekon_log', '/log/pekon_log', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('11', '7', '接口日志', '', '', '', 'api_log', '/log/api_log', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('12', '7', '钩子日志', '', '', '', 'hook_log', '/log/hook_log', '100', '0', '', 'self', '0', '1635916026', '1');
INSERT INTO `flycat_system_menu` VALUES ('13', '0', '平台业务', '', '', '', 'work', '/work', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('14', '13', '品牌管理', '', '', '', 'brand', '/work/brand', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('15', '13', '活动管理', '', '', '', 'activity', '/work/activity', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('16', '13', '分组管理', '', '', '', 'group', '/work/group', '100', '0', '', 'self', '0', '1633919248', '1');
INSERT INTO `flycat_system_menu` VALUES ('17', '13', '奖品设置', '', '', '', 'prize', '/work/prize', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('18', '13', '卡券管理', '', '', '', 'coupon', '/work/coupon', '100', '0', '', 'self', '0', '1635919271', '1');
INSERT INTO `flycat_system_menu` VALUES ('19', '13', '钩子管理', '', '', '', 'hook', '/work/hook', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('20', '0', '业务数据', '', '', '', 'data', '/data', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('21', '20', '用户数据', '', '', '', 'user_data', '/data/user_data', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('22', '20', '卡券数据', '', '', '', 'crad_data', '/data/crad_data', '100', '0', '', 'self', '0', '0', '1');
INSERT INTO `flycat_system_menu` VALUES ('23', '13', '门店管理', '', '', '', 'store', '/work/store', '100', '0', '', 'self', '0', '1633918584', '1');
INSERT INTO `flycat_system_menu` VALUES ('24', '0', 'test', '', '', '', 'test', '123', '100', '0', '', 'self', '1636525222', '1636525222', '1');

-- ----------------------------
-- Table structure for flycat_system_role
-- ----------------------------
DROP TABLE IF EXISTS `flycat_system_role`;
CREATE TABLE `flycat_system_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '角色名称',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '角色描述',
  `rules` text COMMENT '菜单权限',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of flycat_system_role
-- ----------------------------
INSERT INTO `flycat_system_role` VALUES ('1', '超级管理员', '系统默认创建的角色，拥有最高权限', '1,2,3,4,5', '0', '0', '0', '1');
INSERT INTO `flycat_system_role` VALUES ('2', '测试1', '测试的角色1', '1,2,3,4,5', '100', '0', '1632902940', '0');
INSERT INTO `flycat_system_role` VALUES ('4', '你好啊', '纳斯大囧撒', null, '100', '0', '1633766676', '0');
INSERT INTO `flycat_system_role` VALUES ('5', '再来一个', '1232313', null, '100', '0', '1633766680', '0');
